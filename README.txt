-- SUMMARY --

Block Delay addresses the issue of slow loading banners. It prints the contents 
of selected blocks hidden in the footer and calls a javascript function that swaps 
the content with the placeholder (block) which is empty on page load. 
The result is that the selected blocks will not load until the whole page is 
rendered making the page load faster.

The user interface consists only of an extra checkbox in the block settings form.


-- REQUIREMENTS --

* None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Check "Delay block content" in the block settings form 
  (admin/build/block/configure/block/{id}) of those blocks you want to delay