/**
 * Function to replace the contents of "placeholder" with the content of "content"
 */
function blockDelaySwap(placeholder_id, content_id) {
  var content = $('#'+content_id);
  if (content) {
    $('#'+content_id).children("script").remove();
    $('#'+content_id).children("noscript").remove();
    content.parent().remove('#'+content_id);
    var placeholder = $('#'+placeholder_id);
    if (placeholder) {
      placeholder.replaceWith(content.html());
    }
  }
}